#include <malloc.h>
#include <locale.h>
#include <stdio.h>
#include <strings.h>


#define BUFFER_LEN 1000

int main(){
	setlocale(LC_ALL,"Russian");

	// ��������� ������ ��� �������� ������
	char* src = (char*) calloc(BUFFER_LEN, sizeof(char));
	// ���������� ����������
	int repeat_count = 2;

	printf("������� ������\n> ");
	scanf("%s", src);

	if(strlen(src) > BUFFER_LEN){
		printf("������ ������������ �������");
		return 0;
	}

	// ����������� �������� ���������� ����������
	printf("������ ������� ���������� ����������? [Y=1/N=any]\n> ");
	int chose;
	scanf("%d",&chose);
	if(chose == 1){
		printf("������� ������\n> ");
		scanf("%d",&repeat_count);
	}

	long long n = strlen(src) * repeat_count;
	char *target = (char*) calloc(n + 1, sizeof(char));
	char *p_target = target, *p_src = src;

	while(n + target - p_target){
		for(int i = 0; i < repeat_count; i++) *p_target++ = *p_src;
		p_src++;
	}

	printf("����� ������:\n%s",target);
	free(target);
	free(src);
	return 0;
}